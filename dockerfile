ARG IMG=python:3.12-slim
FROM $IMG

# Install your package
RUN apt update && apt install -y git && apt clean
ARG TMP=/tmp/python_project
COPY . $TMP
RUN pip install $TMP && rm -rf $TMP

# Add a non-root user (required in production)
ARG USER=nonroot
RUN adduser $USER
USER $USER
ENV HOME=/home/$USER
WORKDIR $HOME

# Note that you may also want to use "DEBUG" sometimes
ENV LOGLEVEL=INFO
