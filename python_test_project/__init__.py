"""Python test project."""

from importlib.metadata import version, PackageNotFoundError

try:
    __version__ = version("python_test_project")
except PackageNotFoundError:
    pass
