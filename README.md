# Python code quality

Provides various tools to facilitate code quality improvements.

## CI include in other repos

As a basis, the CI scripts contained in this repo can be included to allow factorisation.

```
# TODO:

```

## Description

### Static analysis docker image

To avoid having to download and install necessary packages at every job run, an image is created in this repo and is automatically used.
